package com.cricket.fan.entertainment.ui.utils


enum class UiStatus {
    SUCCESS,
    LOADING,
    NO_DATA,
    CUSTOME_ERROR,
    NETWORK_ERROR,
    SERVER_ERROR
}
