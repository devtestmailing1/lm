package com.example.lmove.network.local;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.lmove.ui.model.ListResponse;

@Database(entities = {ListResponse.class}, version = 1, exportSchema = false)
public abstract class ProjectDatabase extends RoomDatabase {
    private static ProjectDatabase appDatabase;

    public abstract ProjectDataDAO getProjectDAO();


}
