package com.example.lmove.network.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.paging.PageKeyedDataSource
import com.cricket.fan.entertainment.ui.utils.UiStatus
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.lmove.R
import com.example.lmove.network.ResponseObserver
import com.example.lmove.network.local.ProjectDataDAO
import com.example.lmove.network.remote.ApiService
import com.example.lmove.ui.model.ListResponse
import io.reactivex.functions.Action
import retrofit2.Response

class PagingDataRepository
constructor(service: ApiService, dbdata: ProjectDataDAO, compositeDisposable: CompositeDisposable) :
    PageKeyedDataSource<Int, ListResponse>() {
    private var apiService: ApiService
    private var pastdata: ProjectDataDAO
    private var disposable: CompositeDisposable
    public var status: MutableLiveData<UiStatus> = MutableLiveData()
    private var retryCompletable: Completable? = null

    init {
        this.pastdata = dbdata
        this.apiService = service
        this.disposable = compositeDisposable


    }

    override fun loadInitial(
        params: PageKeyedDataSource.LoadInitialParams<Int>,
        callback: PageKeyedDataSource.LoadInitialCallback<Int, ListResponse>
    ) {
        updateState(UiStatus.LOADING)

        apiService.getListData(20,0).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ResponseObserver<List<ListResponse>>(disposable) {

                override fun onNext(t: Response<List<ListResponse>>) {
                    updateState(UiStatus.SUCCESS)
                    //data.postValue(value)
                    pastdata.insertAll(t.body())
                    t.body()?.let { callback.onResult(it, null, 1) }
                }

                override fun onNoData() {
                    updateState(UiStatus.NO_DATA)
                }

                override fun onNetworkError(e: Throwable) {
                    updateState(UiStatus.SUCCESS)
                    callback.onResult(pastdata.projectData, null, 1)

                    //updateState(UiStatus.NETWORK_ERROR)
                }

                override fun onServerError(e: Throwable, code: Int) {
                    updateState(UiStatus.SERVER_ERROR)
                }

            })

    }

    override fun loadBefore(
        params: PageKeyedDataSource.LoadParams<Int>,
        callback: PageKeyedDataSource.LoadCallback<Int, ListResponse>
    ) {

    }

    override fun loadAfter(
        params: PageKeyedDataSource.LoadParams<Int>,
        callback: PageKeyedDataSource.LoadCallback<Int, ListResponse>
    ) {
        updateState(UiStatus.LOADING)
        apiService.getListData(20,params.key).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ResponseObserver<List<ListResponse>>(disposable) {

                override fun onNext(value: Response<List<ListResponse>>) {
                    updateState(UiStatus.SUCCESS)
                    value.body()?.let { callback.onResult(it, params.key + 20) }
                }

                override fun onNoData() {
                    updateState(UiStatus.NO_DATA)
                }

                override fun onNetworkError(e: Throwable) {
                    updateState(UiStatus.NETWORK_ERROR)
                }

                override fun onServerError(e: Throwable, code: Int) {
                    updateState(UiStatus.SERVER_ERROR)
                }
            })
    }

    private fun updateState(state: UiStatus) {
        this.status.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            disposable.add(
                retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            )
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}
