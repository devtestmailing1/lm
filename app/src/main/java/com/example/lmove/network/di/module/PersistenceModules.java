package com.example.lmove.network.di.module;

import android.app.Application;

import androidx.room.Room;

import com.example.lmove.network.local.ProjectDataDAO;
import com.example.lmove.network.local.ProjectDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class PersistenceModules {

    @Provides
    @Singleton
    public ProjectDatabase provicedatabase(Application application) {
        return Room.databaseBuilder(application, ProjectDatabase.class, "mydatabase.db").allowMainThreadQueries().fallbackToDestructiveMigration().build();
    }

    @Provides
    @Singleton
    ProjectDataDAO provideHomeDao(@androidx.annotation.NonNull ProjectDatabase database) {
        return database.getProjectDAO();
    }

}
