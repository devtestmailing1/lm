package com.example.lmove.network.di;

import android.app.Application;

import com.example.lmove.AppController;
import com.example.lmove.network.di.module.ActivityModule;
import com.example.lmove.network.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, AppModule.class, ActivityModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();

    }

    void inject(AppController appApplication);


}

