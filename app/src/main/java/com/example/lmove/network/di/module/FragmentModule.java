package com.example.lmove.network.di.module;

import com.example.lmove.ui.fragments.HomeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {


    @ContributesAndroidInjector
    abstract HomeFragment contributehomefrag();


}
