package com.example.lmove.network;

import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;
import retrofit2.Response;

public abstract class ResponseObserver<T> implements Observer<Response<T>> {

    private int statusCode;

    private CompositeDisposable disposable;

    public ResponseObserver(CompositeDisposable disposable) {
        this.disposable = disposable;
    }

    @Override
    public void onSubscribe(Disposable d) {
        disposable.add(d);
    }

   /* @Override
    public void onNext(Response<T> response) {
        Log.e("TAG", "Response header code is " + response.code());
        if (response.code() == HTTP_OK) {
            onSuccess(response);
        } else if (response.code() == HTTP_NO_CONTENT) {
            onNoData();
        } else {
            onServerError(new HttpException(response), response.code());
        }

    }*/

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            statusCode = ((HttpException) e).response().code();
            onServerError(e, statusCode);
        } else {
            onNetworkError(e);
        }

        Log.e("TAG"," throw eception "+e.getMessage());
    }

    @Override
    public void onComplete() {

    }

    //public abstract void onSuccess(T response);

    public abstract void onNoData();

    public abstract void onNetworkError(Throwable e);

    /*called when api Http_code is not 200*/
    public abstract void onServerError(Throwable e, int code);
}
