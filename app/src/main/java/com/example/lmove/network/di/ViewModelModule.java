package com.example.lmove.network.di;

import androidx.lifecycle.ViewModel;

import com.example.lmove.network.viewmodel.ViewModelKey;
import com.example.lmove.ui.viewmodel.ListViewModel;
import com.example.lmove.ui.viewmodel.PagingViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@SuppressWarnings("WeakerAccess")
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel.class)
    abstract ViewModel bindListViewModel(ListViewModel listViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PagingViewModel.class)
    abstract ViewModel bindPagingListViewModel(PagingViewModel listViewModel);


}
