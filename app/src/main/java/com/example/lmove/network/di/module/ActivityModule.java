package com.example.lmove.network.di.module;

import com.example.lmove.ui.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {


    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract MainActivity contributes();


}
