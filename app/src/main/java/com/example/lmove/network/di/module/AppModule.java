package com.example.lmove.network.di.module;

import android.app.Application;
import android.net.Uri;

import com.example.lmove.BuildConfig;
import com.example.lmove.network.di.ViewModelModule;
import com.example.lmove.network.remote.ApiService;
import com.example.lmove.utils.CommanUtils;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.BitSet;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {ViewModelModule.class, PersistenceModules.class})
public class AppModule {
    public final int TIMEOUT_IN_SEC = 15;


    @Singleton
    @Provides
    ApiService provideGithubService(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService.class);
    }


    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    public StethoInterceptor getSteltho() {
        return new StethoInterceptor();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Application application, StethoInterceptor stethoInterceptor, HttpLoggingInterceptor httpLoggingInterceptor) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.readTimeout(TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.addInterceptor(getIntercepter(application));
        if (BuildConfig.DEBUG) {
            okHttpClient.addNetworkInterceptor(stethoInterceptor);
            okHttpClient.interceptors().add(httpLoggingInterceptor);
        }

        return okHttpClient.build();
    }

    public Interceptor getIntercepter(final Application application) {

        Interceptor headerAuthorizationInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                // Log.e("TAG", String.format("req response cache raw JSON response "));
                // String token=manager.getPrefrenceData(HEADER_TOKEN);
                if (!CommanUtils.isNetworkAvailable()) {
                    Request request = chain.request();
                    CacheControl cacheControl = new CacheControl.Builder().maxStale(1, TimeUnit.DAYS).build();
                    request = request.newBuilder().cacheControl(cacheControl).build();
                    String rawJson = chain.proceed(request).body().string();
                    //Log.e("TAG", String.format("req response form cache response is: %s", rawJson));
                    return chain.proceed(request);
                } else {
                    // Log.e("TAG", String.format("req response from server "));
                    CacheControl cacheControl = new CacheControl.Builder().maxAge(1, TimeUnit.HOURS).build();
                    Request.Builder request = chain.request().newBuilder();
                    request.addHeader("Accept", "application/json");
                    // request.addHeader("Authorization", "Bearer " + token);
                    request.header("cache-Control", cacheControl.toString());
                    Response response = chain.proceed(request.build());
                    return response;
                }
            }
        };
        return headerAuthorizationInterceptor;
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

}
