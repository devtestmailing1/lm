package com.cricket.fan.entertainment.ui.view.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.lmove.network.local.ProjectDataDAO
import com.example.lmove.network.remote.ApiService
import com.example.lmove.network.viewmodel.PagingDataRepository
import com.example.lmove.ui.model.ListResponse
import io.reactivex.disposables.CompositeDisposable


class RecentDataSorceFactory(private val networkService: ApiService, private val dbdata: ProjectDataDAO, private val compositeDisposable: CompositeDisposable) : DataSource.Factory<Int, ListResponse>() {
    val newsDataSourceLiveData = MutableLiveData<PagingDataRepository>()

    override fun create(): DataSource<Int, ListResponse> {
        Log.e("TAG"," recent step 2")
        val newsDataSource = PagingDataRepository(networkService,dbdata, compositeDisposable)
        newsDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}
