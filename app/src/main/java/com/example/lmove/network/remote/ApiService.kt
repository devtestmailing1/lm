package com.example.lmove.network.remote

import com.example.lmove.BuildConfig
import com.example.lmove.ui.model.ListResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(BuildConfig.API_ENDS)
    fun getListData(@Query("limit") page: Int,@Query("offset")param2:Int): Observable<Response<List<ListResponse>>>

}