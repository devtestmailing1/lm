package com.example.lmove.network.local;

import androidx.lifecycle.LiveData;
import androidx.room.*;

import com.example.lmove.ui.model.ListResponse;

import java.util.List;

import kotlin.jvm.JvmOverloads;
import retrofit2.http.Query;

@Dao
public interface ProjectDataDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public Long[] saveProject(ListResponse... projects);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<ListResponse> note);


    @Update
    public void updateProject(ListResponse... projects);

    @androidx.room.Query("DELETE FROM l_move")
    public void deleteAll();

    @Delete
    public void deleteProject(ListResponse project);

    @androidx.room.Query("DELETE FROM l_move WHERE Id = :id")
    public void deleteWithIdes(int id);


    @androidx.room.Query("SELECT * FROM l_move")
    public List<ListResponse> getProjectData();

    @androidx.room.Query("UPDATE l_move SET url=:titles WHERE Id = :id")
    public void updateDataWithIdes(String titles, int id);


    @androidx.room.Query("SELECT * FROM l_move WHERE Id = :id")
    public LiveData<List<ListResponse>> getProjectList(String id);

}
