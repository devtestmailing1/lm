package com.example.lmove.ui.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowId
import androidx.lifecycle.Observer

import com.example.lmove.network.di.Injectable
import javax.inject.Inject
import com.example.lmove.ui.viewmodel.ViewModelFactory
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_home.*
import com.cricket.fan.entertainment.ui.utils.UiStatus
import com.example.lmove.interfaces.OnclickAction
import com.example.lmove.ui.MainActivity
import com.example.lmove.ui.adapter.PagingListAdapter
import com.example.lmove.ui.viewmodel.PagingViewModel
import android.R
import android.R.attr.colorPrimary
import android.R.attr.colorPrimary





class HomeFragment : Fragment(), Injectable, OnclickAction, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: PagingViewModel
    private lateinit var layoutManager: LinearLayoutManager
    lateinit var view1: View
    lateinit var mSwipeRefreshLayout:SwipeRefreshLayout
    var error:String = "no"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        view1= inflater.inflate(com.example.lmove.R.layout.fragment_home, container, false)
        return view1
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeRefreshLayout()
    }


    private fun swipeRefreshLayout(){

        handleError()
        mSwipeRefreshLayout = swipe_container as SwipeRefreshLayout
        mSwipeRefreshLayout.setOnRefreshListener(this)
        mSwipeRefreshLayout.setColorSchemeResources(
            android.R.color.holo_red_light,
            android.R.color.holo_green_dark,
            android.R.color.holo_orange_dark,
            android.R.color.holo_blue_dark)

        mSwipeRefreshLayout.post {
            getDataListWithpaging()
        }

    }

    private fun getDataListWithpaging() {

        progrss.visibility = View.VISIBLE
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PagingViewModel::class.java)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
        recyclerview.layoutManager=layoutManager


        viewModel.initViewMode()

        val tripAdapter = PagingListAdapter(context,this)
        recyclerview.adapter = tripAdapter

        viewModel.newsList.observe(this, Observer {
            progrss.visibility = View.GONE
            tripAdapter.submitList(it)
        })

     initState(viewModel,tripAdapter)

    }

    private fun initState(viewModel: PagingViewModel,adapter: PagingListAdapter) {

        viewModel.getState().observe(this, Observer { state ->
            state?.let { adapter.setState(it) }

            when (state) {
                UiStatus.SUCCESS -> {
                    Log.e("TAG","  Error success")
                }
                UiStatus.NO_DATA -> {
                    Log.e("TAG","  Error no data")
                }
                UiStatus.LOADING -> {
                    Log.e("TAG","  Error loading")
                }
                UiStatus.NETWORK_ERROR -> {
                    error = "yes"
                    Log.e("TAG","  Error network error")
                }
                UiStatus.SERVER_ERROR -> {
                    error = "yes"
                    Log.e("TAG","  Error server error")
                }
                else -> Log.e("TAG","Something went worng.")
            }

            mSwipeRefreshLayout.isRefreshing = false
        })
    }

    override fun onActionClick(id:String) {
        (activity as MainActivity).onActionClickk(id)
    }

    override fun onRefresh() {
        getDataListWithpaging()
    }

    private fun handleError(){
        if (error.equals("yes")){
            reload.visibility = View.VISIBLE
        }
        reload.setOnClickListener(View.OnClickListener {
            swipeRefreshLayout()
        })
    }



}
