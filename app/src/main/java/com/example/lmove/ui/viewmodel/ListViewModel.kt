package com.example.lmove.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.cricket.fan.entertainment.network.di.DataRepository
import com.cricket.fan.entertainment.ui.view.viewmodel.RecentDataSorceFactory
import com.example.lmove.network.ResponseObserver
import com.example.lmove.network.local.ProjectDataDAO
import com.example.lmove.network.remote.ApiService
import com.example.lmove.ui.model.ListResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject


class ListViewModel
@Inject
constructor(
    api: ApiService,
    repository: DataRepository,
    cdisposable: CompositeDisposable
   // localdb: ProjectDataDAO
) : ViewModel() {

    private lateinit var apiService: ApiService
    private lateinit var disposable: CompositeDisposable

    var pageSize: Int = 10
    lateinit var newsList: LiveData<PagedList<ListResponse>>
    private lateinit var dataSourceFactory: RecentDataSorceFactory

    var datalist: MutableLiveData<List<ListResponse>> = MutableLiveData()
    private val isLoadingg = MutableLiveData<Boolean>()

    init {
        this.apiService = api
        this.disposable = cdisposable
        repository.requestForList(0, datalist, isLoadingg)
    }


   /* public fun init(page: Int) {
        //dataSourceFactory = RecentDataSorceFactory( apiService, disposable)

        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()

        Log.d("newslist>", "called");

        newsList = LivePagedListBuilder<Int, ListResponse>(dataSourceFactory, config).build()
    }*/

    public fun getdata() {
        Log.e("TAG", " step 1")

        apiService.getListData(10,20).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ResponseObserver<List<ListResponse>>(disposable) {
                override fun onNext(value: Response<List<ListResponse>>) {
                    Log.e("TAG", " my response> " + value.body()?.size)
                    datalist.postValue(value.body())
                }

                override fun onNoData() {
                    Log.e("TAG", " step 2")
                }

                override fun onNetworkError(e: Throwable) {
                    Log.e("TAG", " step 3")
                }

                override fun onServerError(e: Throwable, code: Int) {
                    Log.e("TAG", " step 1")
                }

            })
    }


}