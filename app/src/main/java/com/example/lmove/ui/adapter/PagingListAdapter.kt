package com.example.lmove.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cricket.fan.entertainment.ui.utils.UiStatus
import com.example.lmove.R
import com.example.lmove.databinding.ListItemBinding
import com.example.lmove.interfaces.OnclickAction
import com.example.lmove.ui.MainActivity
import com.example.lmove.ui.fragments.HomeFragment
import com.example.lmove.ui.model.ListResponse
import com.example.mvvmkotlindatabinding.utils.extension.getParentActivity
import kotlinx.android.synthetic.main.item_footer.view.*
import kotlinx.android.synthetic.main.list_item.view.*

class PagingListAdapter(ctxx: Context?,onclickAction1: OnclickAction) : PagedListAdapter<ListResponse, RecyclerView.ViewHolder>(NewsDiffCallback) {

    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2
    private var context: Context? = null
    var mClickListener: OnclickAction? = null



    private var state = UiStatus.LOADING

    init {
        this.context = ctxx
        this.mClickListener = onclickAction1
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == DATA_VIEW_TYPE) MatchViewHolder.create(parent) else MatchFooterViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            getItem(position)?.let {
                context?.let { it1 ->
                    (holder as MatchViewHolder).bind(
                        it,
                        it1,
                        mClickListener
                    )
                }
            }
        else (holder as MatchFooterViewHolder).bind(state)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    companion object {
        val NewsDiffCallback = object : DiffUtil.ItemCallback<ListResponse>() {
            override fun areItemsTheSame(oldItem: ListResponse, newItem: ListResponse): Boolean {
                return oldItem.id == newItem.id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: ListResponse, newItem: ListResponse): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == UiStatus.LOADING || state == UiStatus.SERVER_ERROR)
    }

    fun setState(state: UiStatus) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }


    class MatchViewHolder(itemViews: ViewDataBinding) : RecyclerView.ViewHolder(itemViews.root) {
        val views=itemViews as ListItemBinding;

        fun bind(listdata: ListResponse, ctx: Context,mClickListener: OnclickAction?) {
            //Glide.with(ctx).load(listdata.imageUrl).into(imageview)
            //tvtitle.text = listdata.description
            itemView.item.setOnClickListener(View.OnClickListener {
                    mClickListener?.onActionClick(listdata.id)
            })
            views.mydata=listdata

        }

        companion object {
            fun create(parent: ViewGroup): MatchViewHolder {
                val employeeListItemBinding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.list_item, parent, false)
                return MatchViewHolder(employeeListItemBinding)
            }
        }
    }


    class MatchFooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(status: UiStatus) {
            itemView.progress_bar.visibility = if (status == UiStatus.LOADING) VISIBLE else View.INVISIBLE
            itemView.txt_error.visibility = if (status == UiStatus.SERVER_ERROR) VISIBLE else View.INVISIBLE
        }

        companion object {
            fun create(parent: ViewGroup): MatchFooterViewHolder {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_footer, parent, false)
                return MatchFooterViewHolder(view)
            }
        }
    }

}
