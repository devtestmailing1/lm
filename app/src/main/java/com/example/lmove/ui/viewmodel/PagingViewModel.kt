package com.example.lmove.ui.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.cricket.fan.entertainment.network.di.DataRepository
import com.cricket.fan.entertainment.ui.utils.UiStatus
import com.cricket.fan.entertainment.ui.view.viewmodel.RecentDataSorceFactory
import com.example.lmove.network.local.ProjectDataDAO
import com.example.lmove.network.remote.ApiService
import com.example.lmove.network.viewmodel.PagingDataRepository
import com.example.lmove.ui.model.ListResponse
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PagingViewModel
@Inject
constructor(
    api: ApiService,
    homedao: ProjectDataDAO,
    projectRepository: DataRepository,
    application: Application,
    dispos: CompositeDisposable
) : ViewModel() {
    private val pageSize = 5
    var apie: ApiService
    var homedao: ProjectDataDAO
    var disposable: CompositeDisposable
    lateinit var newsList: LiveData<PagedList<ListResponse>>

    private lateinit var dataSourceFactory: RecentDataSorceFactory
    lateinit var id: String

    init {
        this.apie = api
        this.homedao = homedao
        this.disposable = dispos


    }

    fun initViewMode() {

        init()

    }

    private fun init() {
        dataSourceFactory = RecentDataSorceFactory(apie, homedao, disposable)

        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()

        newsList = LivePagedListBuilder<Int, ListResponse>(dataSourceFactory, config).build()
    }

    fun getState(): LiveData<UiStatus> =
        Transformations.switchMap<PagingDataRepository, UiStatus>(
            dataSourceFactory.newsDataSourceLiveData, PagingDataRepository::status
        )

    public fun retry() {
        dataSourceFactory.newsDataSourceLiveData.value?.retry()
    }

    fun listIsEmpty(): Boolean {
        return newsList.value?.isEmpty() ?: true
    }

}
