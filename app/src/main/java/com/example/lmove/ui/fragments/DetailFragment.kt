package com.example.lmove.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.R
import android.util.Log


class DetailFragment : Fragment(), OnMapReadyCallback {

    lateinit var googleMap: GoogleMap;
     var lat: Double? = null
     var lng: Double? = null
    var address: String = "";
    private lateinit var userId:String


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(com.example.lmove.R.layout.user_details, container, false)
        return view
    }

    override fun onStart() {
        super.onStart()
        userId = arguments?.get("id") as String

        //Data from Room will come here

        val mapFragment = childFragmentManager
            .findFragmentById(com.example.lmove.R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(DetailFragment())
    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0!!
        lat = 22.319181
        lng = 114.170008
        val place = LatLng(lat!!, lng!!)
        googleMap.addMarker(MarkerOptions().position(place).title(address))
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(place))
    }

}