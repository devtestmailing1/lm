package com.example.lmove.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.lmove.R
import com.example.lmove.ui.fragments.DetailFragment
import com.example.lmove.ui.fragments.HomeFragment
import com.google.android.gms.maps.MapFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasSupportFragmentInjector{

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.lmove.R.layout.activity_main)

        supportFragmentManager.beginTransaction().replace(com.example.lmove.R.id.mycontainer,HomeFragment()).commit()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }




    public fun goToNextFragment(id:String){
        var fragment = DetailFragment()
        var bundle = Bundle()
        bundle.putString("id",id)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(com.example.lmove.R.id.mycontainer,fragment).addToBackStack(fragment.javaClass.name).commit()
    }




     fun onActionClickk(id: String) {
         goToNextFragment(id)
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            super.onBackPressed()
            //additional code
        } else {
            supportFragmentManager.popBackStack()
        }

    }
}
