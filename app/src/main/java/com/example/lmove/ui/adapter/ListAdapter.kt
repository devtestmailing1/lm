package com.example.lmove.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.example.lmove.R
import com.example.lmove.ui.model.ListResponse

class ListAdapter(ctxx: Context?) :
    PagedListAdapter<ListResponse, RecyclerView.ViewHolder>(TripsDiffCallback) {

    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2
    private var context: Context? = null

    init {
        this.context = ctxx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MatchViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        getItem(position)?.let { context?.let { it1 -> (holder as MatchViewHolder).bind(it, it1) } }

    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    companion object {
        val TripsDiffCallback = object : DiffUtil.ItemCallback<ListResponse>() {
            override fun areItemsTheSame(oldItem: ListResponse, newItem: ListResponse): Boolean {
                return oldItem.id == newItem.id
            }


            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: ListResponse, newItem: ListResponse): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }


    class MatchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvDest: AppCompatTextView = itemView.findViewById(R.id.tvdesc)
        private val IvItemImage: AppCompatImageView = itemView.findViewById(R.id.item_image);

        fun bind(listdata: ListResponse, ctx: Context) {
            tvDest.text = listdata.description
            Glide.with(ctx).load(listdata.imageUrl).into(IvItemImage);

        }

        companion object {
            fun create(parent: ViewGroup): MatchViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item, parent, false)
                return MatchViewHolder(view)
            }
        }
    }
}
