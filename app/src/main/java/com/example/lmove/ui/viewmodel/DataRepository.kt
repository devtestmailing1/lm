package com.cricket.fan.entertainment.network.di

import androidx.lifecycle.MutableLiveData
import com.example.lmove.network.ResponseObserver
import com.example.lmove.network.local.ProjectDataDAO
import com.example.lmove.network.remote.ApiService
import com.example.lmove.ui.model.ListResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject


class DataRepository
@Inject
constructor(
    private val gitHubService: ApiService,
    private val disposable: CompositeDisposable,
    localdb: ProjectDataDAO
) {

    fun requestForList(
        request: Int,
        data: MutableLiveData<List<ListResponse>>,
        status: MutableLiveData<Boolean>
    ) {

        status.value = true
        gitHubService.getListData(request,20).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ResponseObserver<List<ListResponse>>(disposable) {
                override fun onNext(value: Response<List<ListResponse>>) {
                    status.value = false
                    data.postValue(value.body())
                }

                override fun onNoData() {
                    status.value = false
                }

                override fun onNetworkError(e: Throwable) {
                    status.value = false
                }

                override fun onServerError(e: Throwable, code: Int) {
                    status.value = false
                }

            })

    }

}
